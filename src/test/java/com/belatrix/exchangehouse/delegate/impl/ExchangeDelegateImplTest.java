package com.belatrix.exchangehouse.delegate.impl;

import com.belatrix.exchangehouse.delegate.IExchangeDelegate;
import com.belatrix.exchangehouse.dto.request.ExchangeRq;
import com.belatrix.exchangehouse.exception.BuildElementException;
import com.belatrix.exchangehouse.exception.ElementNotFoundException;
import com.belatrix.exchangehouse.model.Coin;
import com.belatrix.exchangehouse.model.ExchangeCoins;
import com.belatrix.exchangehouse.service.ICoinsService;
import com.belatrix.exchangehouse.service.IExchangeService;
import com.belatrix.exchangehouse.service.ITransactionService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ExchangeDelegateImplTest {

    @Mock
    private IExchangeService exchangeService;
    @Mock
    private ITransactionService transactionService;
    @Mock
    private ICoinsService coinsService;

    private IExchangeDelegate exchangeDelegate;
    private ExchangeRq exchangeRq;
    private ExchangeCoins exchangeCoins;
    private Coin base;
    private Coin change;

    @BeforeEach
    void setUp() {
        exchangeDelegate = new ExchangeDelegateImpl(exchangeService, transactionService, coinsService);
        exchangeRq = new ExchangeRq(1000.0020,1);
        exchangeCoins = new ExchangeCoins(1,new Coin(), new Coin(),0.90,true);
        base = new Coin(1,"TST","Spring Test");
        change = new Coin(2, "UNT", "Unit Test");
    }

    @Test
    private void getDataExchangeByBase_OK() throws ElementNotFoundException, BuildElementException {
        when(exchangeService.getCoinsToExchangeByBase(anyInt())).thenReturn(Optional.of(Collections.singletonList(exchangeCoins)));
        assertNotNull(exchangeDelegate.getDataExchangeByBase(1));
    }

}