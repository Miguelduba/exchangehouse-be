package com.belatrix.exchangehouse.service.impl;

import com.belatrix.exchangehouse.model.ExchangeCoins;
import com.belatrix.exchangehouse.repository.IExchangeCoinsRepository;
import com.belatrix.exchangehouse.service.IExchangeService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ExchangeServiceImplTest {

    @Mock
    private IExchangeCoinsRepository exchangeCoinsRepository;

    private IExchangeService exchangeService;

    @BeforeEach
    void setUp() {
        exchangeService = new ExchangeServiceImpl(exchangeCoinsRepository);
    }

    @Test
    void getExchangeValue_Found() {
        when(exchangeCoinsRepository.findById(anyInt())).thenReturn(Optional.of(new ExchangeCoins()));
        assertNotNull(exchangeService.getExchange(1));
    }

    @Test
    void getExchangeValue_NotFound() {
        when(exchangeCoinsRepository.findById(anyInt())).thenReturn(Optional.of(new ExchangeCoins()));
        assertNotNull(exchangeService.getExchange(2));
    }
}