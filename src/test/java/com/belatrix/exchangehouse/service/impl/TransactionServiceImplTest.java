package com.belatrix.exchangehouse.service.impl;

import com.belatrix.exchangehouse.model.Transaction;
import com.belatrix.exchangehouse.repository.ITransactionsRepository;
import com.belatrix.exchangehouse.service.ITransactionService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class TransactionServiceImplTest {

    @Mock
    private ITransactionsRepository transactionsRepository;

    private ITransactionService transactionService;


    @BeforeEach
    void setUp() {
        transactionService = new TransactionServiceImpl(transactionsRepository);
    }

    @Test
    void saveTransaction_Save() {
        when(transactionsRepository.save(any(Transaction.class))).thenReturn(new Transaction());
        transactionService.saveTransaction(new Transaction());
    }

    @Test
    void saveTransaction_NotSave() {
        when(transactionsRepository.save(any(Transaction.class))).thenReturn(null);
        transactionService.saveTransaction(new Transaction());
    }

    @Test
    void getAllTransactions() {
        when(transactionsRepository.findAll()).thenReturn(Arrays.asList(new Transaction(), new Transaction(), new Transaction()));
        assertNotNull(transactionService.getAllTransactions());
    }
}