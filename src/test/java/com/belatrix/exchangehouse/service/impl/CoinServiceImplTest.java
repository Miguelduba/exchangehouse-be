package com.belatrix.exchangehouse.service.impl;

import com.belatrix.exchangehouse.model.Coin;
import com.belatrix.exchangehouse.repository.ICoinsRepository;
import com.belatrix.exchangehouse.service.ICoinsService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CoinServiceImplTest {

    @Mock
    private ICoinsRepository coinsRepository;

    private ICoinsService coinsService;

    @BeforeEach
    void setUp() {
        coinsService = new CoinServiceImpl(coinsRepository);
    }

    @Test
    void getCoin_Found() {
        when(coinsRepository.findById(anyInt())).thenReturn(Optional.of(new Coin()));
        assertNotNull(coinsService.getCoin(1));
    }

    @Test
    void getCoin_NotFound() {
        when(coinsRepository.findById(anyInt())).thenReturn(Optional.empty());
        assertNull(coinsService.getCoin(2));
    }
}