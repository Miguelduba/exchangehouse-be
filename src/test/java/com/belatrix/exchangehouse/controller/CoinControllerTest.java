package com.belatrix.exchangehouse.controller;

import com.belatrix.exchangehouse.constant.RequestConstants;
import com.belatrix.exchangehouse.delegate.ICoinsDelegate;
import com.belatrix.exchangehouse.model.Coin;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;

import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest
@AutoConfigureMockMvc
class CoinControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private ICoinsDelegate coinDelegate;

    private CoinController coinController;

    @BeforeEach
    void setUp() {
        coinController = new CoinController(coinDelegate);
    }

    @Test
    void getAvailableCoins() throws Exception {
        when(coinDelegate.getAllCoins()).thenReturn(Arrays.asList(new Coin()));
        mvc.perform(get(RequestConstants.COINS)
                .contentType(APPLICATION_JSON))
                .andExpect(status().isOk());
    }
}