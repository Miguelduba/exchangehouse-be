package com.belatrix.exchangehouse.controller;

import com.belatrix.exchangehouse.constant.RequestConstants;
import com.belatrix.exchangehouse.delegate.IExchangeDelegate;
import com.belatrix.exchangehouse.dto.response.ExchangeDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Collections;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest
@AutoConfigureMockMvc
class ExchangeControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private IExchangeDelegate exchangeDelegate;

    private ExchangeController exchangeController;

    @BeforeEach
    void setUp() {
        exchangeController = new ExchangeController(exchangeDelegate);
    }

    @Test
    void calculateChange() throws Exception {
        when(exchangeDelegate.getDataExchangeByBase(anyInt())).thenReturn(Collections.singletonList(new ExchangeDto()));
        mvc.perform(get(RequestConstants.EXCHANGE + "/1")
            .contentType(APPLICATION_JSON))
                .andExpect(status().isOk());
    }
}