package com.belatrix.exchangehouse.exception;

public class BuildElementException extends RuntimeException {

    public BuildElementException(String message) {
        super(message);
    }
}
