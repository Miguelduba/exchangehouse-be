package com.belatrix.exchangehouse.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "exchange_coins")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ExchangeCoins {
    @Id
    private Integer id;
    @ManyToOne
    @JoinColumn(name = "base", insertable = false, updatable = false)
    private Coin base;
    @ManyToOne
    @JoinColumn(name = "change", insertable = false, updatable = false)
    private Coin change;
    private Double value;
    private Boolean active;
}
