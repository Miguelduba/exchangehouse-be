package com.belatrix.exchangehouse.delegate;

import com.belatrix.exchangehouse.dto.response.ExchangeDto;
import com.belatrix.exchangehouse.dto.response.ExchangeRs;
import com.belatrix.exchangehouse.exception.BuildElementException;
import com.belatrix.exchangehouse.exception.ElementNotFoundException;

import java.util.List;

public interface IExchangeDelegate {

    List<ExchangeDto> getDataExchangeByBase(Integer id) throws ElementNotFoundException, BuildElementException;

    ExchangeRs getExchange(Integer id) throws BuildElementException, ElementNotFoundException;

}
