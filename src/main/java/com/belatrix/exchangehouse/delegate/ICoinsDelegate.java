package com.belatrix.exchangehouse.delegate;

import com.belatrix.exchangehouse.model.Coin;

import java.util.List;

public interface ICoinsDelegate {

    List<Coin> getAllCoins();
}
