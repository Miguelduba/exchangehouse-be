package com.belatrix.exchangehouse.delegate.impl;

import com.belatrix.exchangehouse.delegate.IExchangeDelegate;
import com.belatrix.exchangehouse.dto.response.ExchangeDto;
import com.belatrix.exchangehouse.dto.response.ExchangeRs;
import com.belatrix.exchangehouse.exception.BuildElementException;
import com.belatrix.exchangehouse.exception.ElementNotFoundException;
import com.belatrix.exchangehouse.model.ExchangeCoins;
import com.belatrix.exchangehouse.service.ICoinsService;
import com.belatrix.exchangehouse.service.IExchangeService;
import com.belatrix.exchangehouse.service.ITransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ExchangeDelegateImpl implements IExchangeDelegate  {

    private IExchangeService exchangeService;
    private ITransactionService transactionService;
    private ICoinsService coinsService;

    @Autowired
    public ExchangeDelegateImpl(IExchangeService exchangeService, ITransactionService transactionService, ICoinsService coinsService) {
        this.exchangeService = exchangeService;
        this.transactionService = transactionService;
        this.coinsService = coinsService;
    }

    @Override
    public List<ExchangeDto> getDataExchangeByBase(Integer baseId) {
        List<ExchangeDto> exchangeDtos;
        Optional<List<ExchangeCoins>> exchangeCoin = exchangeService.getCoinsToExchangeByBase(baseId);
        if (!exchangeCoin.isPresent())
            throw new ElementNotFoundException("Exchange Elements to "+ baseId + " not found");
        try {
            exchangeDtos = exchangeCoin.get()
                    .stream()
                    .map(elem -> new ExchangeDto(elem.getId(), elem.getChange().getAcronym()))
                    .collect(Collectors.toList());
        }catch (Exception ex){
            throw new BuildElementException("Couldn't build response");
        }
        return exchangeDtos;
    }

    @Override
    public ExchangeRs getExchange(Integer id) {
        ExchangeRs exchangeRs;
        Optional<ExchangeCoins> exchangeCoin = exchangeService.getExchange(id);
        if (!exchangeCoin.isPresent())
            throw new ElementNotFoundException("Exchange Elements to "+ id + " not found");
        try {
            ExchangeCoins exchangeData = exchangeCoin.get();
            exchangeRs = new ExchangeRs(exchangeData.getBase().getAcronym(), exchangeData.getChange().getAcronym(), exchangeData.getValue());
        }catch (Exception ex){
            throw new BuildElementException("Couldn't build response");
        }

        return exchangeRs;
    }

}
