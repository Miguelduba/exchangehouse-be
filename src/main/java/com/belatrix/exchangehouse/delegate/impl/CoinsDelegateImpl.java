package com.belatrix.exchangehouse.delegate.impl;

import com.belatrix.exchangehouse.delegate.ICoinsDelegate;
import com.belatrix.exchangehouse.exception.ElementNotFoundException;
import com.belatrix.exchangehouse.model.Coin;
import com.belatrix.exchangehouse.service.ICoinsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class CoinsDelegateImpl implements ICoinsDelegate {

    private ICoinsService coinsService;

    @Autowired
    public CoinsDelegateImpl(ICoinsService coinsService) {
        this.coinsService = coinsService;
    }


    @Override
    public List<Coin> getAllCoins() {
        List<Coin> coins = coinsService.getAllCoins();
        if (coins == null)
            throw new ElementNotFoundException("Coins not Found");
        return coins;
    }
}
