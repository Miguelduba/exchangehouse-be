package com.belatrix.exchangehouse;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExchangehouseApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExchangehouseApplication.class, args);
	}

}
