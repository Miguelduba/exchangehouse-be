package com.belatrix.exchangehouse.repository;

import com.belatrix.exchangehouse.model.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ITransactionsRepository extends JpaRepository<Transaction, Integer> {
}
