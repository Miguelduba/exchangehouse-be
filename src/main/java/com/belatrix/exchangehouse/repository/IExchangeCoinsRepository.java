package com.belatrix.exchangehouse.repository;

import com.belatrix.exchangehouse.model.ExchangeCoins;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IExchangeCoinsRepository extends JpaRepository<ExchangeCoins, Integer> {

    List<ExchangeCoins> getAllByBase(Integer base);
}
