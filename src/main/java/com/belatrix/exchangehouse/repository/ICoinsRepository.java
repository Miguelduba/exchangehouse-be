package com.belatrix.exchangehouse.repository;

import com.belatrix.exchangehouse.model.Coin;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ICoinsRepository extends JpaRepository<Coin, Integer> {
}
