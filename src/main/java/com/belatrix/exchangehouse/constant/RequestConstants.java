package com.belatrix.exchangehouse.constant;

public class RequestConstants {
    public static final String EXCHANGE = "/exchange";
    public static final String EXCHANGES = "/exchanges";
    public static final String COINS = "/coins";
    public static final String TRANSACTION = "/transaction";
}
