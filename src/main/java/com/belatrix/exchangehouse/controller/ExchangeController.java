package com.belatrix.exchangehouse.controller;

import com.belatrix.exchangehouse.constant.RequestConstants;
import com.belatrix.exchangehouse.delegate.IExchangeDelegate;
import com.belatrix.exchangehouse.dto.response.ExchangeDto;
import com.belatrix.exchangehouse.dto.response.ExchangeRs;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@Controller
public class ExchangeController {

    private IExchangeDelegate exchangeDelegate;

    @Autowired
    public ExchangeController(IExchangeDelegate exchangeDelegate) {
        this.exchangeDelegate = exchangeDelegate;
    }

    @GetMapping(value =  RequestConstants.EXCHANGES + "/{coinId}")
    public ResponseEntity<List<ExchangeDto>> getAvailableExchange(@PathVariable Integer coinId) {
        return ResponseEntity.ok(exchangeDelegate.getDataExchangeByBase(coinId));
    }

    @GetMapping(value = RequestConstants.EXCHANGE + "/{exchangeId}")
    public ResponseEntity<ExchangeRs> getDataExchange(@PathVariable Integer exchangeId) {
        return new ResponseEntity<>(exchangeDelegate.getExchange(exchangeId), HttpStatus.OK);
    }
}
