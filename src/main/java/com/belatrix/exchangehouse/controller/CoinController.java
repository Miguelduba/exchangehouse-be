package com.belatrix.exchangehouse.controller;

import com.belatrix.exchangehouse.constant.RequestConstants;
import com.belatrix.exchangehouse.delegate.ICoinsDelegate;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class CoinController {

    private ICoinsDelegate coinsDelegate;

    public CoinController(ICoinsDelegate coinsDelegate) {
        this.coinsDelegate = coinsDelegate;
    }

    @GetMapping(RequestConstants.COINS)
    public ResponseEntity getAvailableCoins() {
        return ResponseEntity.ok(coinsDelegate.getAllCoins());
    }

}
