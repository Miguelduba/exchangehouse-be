package com.belatrix.exchangehouse.handler;

import com.belatrix.exchangehouse.exception.BuildElementException;
import com.belatrix.exchangehouse.exception.ElementNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@Slf4j
@RestControllerAdvice
public class ChangeHouseExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = {ElementNotFoundException.class, BuildElementException.class})
    public final ResponseEntity<String> handlerNotFoundElement(RuntimeException ex){
        if (ex instanceof ElementNotFoundException){
            return new ResponseEntity<>(ex.getMessage(), HttpStatus.BAD_REQUEST);
        }
        if (ex instanceof BuildElementException){
            return new ResponseEntity<>(ex.getMessage(), HttpStatus.CONFLICT);
        }
        return new ResponseEntity<>(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
