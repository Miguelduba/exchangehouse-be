package com.belatrix.exchangehouse.service;

import com.belatrix.exchangehouse.model.Coin;

import java.util.List;

public interface ICoinsService {

    Coin getCoin(Integer idCoin);

    List<Coin> getAllCoins();
}
