package com.belatrix.exchangehouse.service;

import com.belatrix.exchangehouse.model.ExchangeCoins;

import java.util.List;
import java.util.Optional;

public interface IExchangeService {

    Optional<ExchangeCoins> getExchange(Integer idExchange);

    Optional<List<ExchangeCoins>> getCoinsToExchangeByBase(Integer baseId);

}
