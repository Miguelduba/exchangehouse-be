package com.belatrix.exchangehouse.service.impl;

import com.belatrix.exchangehouse.model.Coin;
import com.belatrix.exchangehouse.repository.ICoinsRepository;
import com.belatrix.exchangehouse.service.ICoinsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class CoinServiceImpl implements ICoinsService {

    private ICoinsRepository coinsRepository;

    @Autowired
    public CoinServiceImpl(ICoinsRepository coinsRepository) {
        this.coinsRepository = coinsRepository;
    }

    @Override
    public Coin getCoin(Integer idCoin) {
        Optional<Coin> foundCoin = coinsRepository.findById(idCoin);
        return foundCoin.orElse(null);
    }

    public List<Coin> getAllCoins(){
        return coinsRepository.findAll();
    }

}
