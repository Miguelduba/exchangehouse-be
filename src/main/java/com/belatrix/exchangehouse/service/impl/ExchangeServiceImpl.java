package com.belatrix.exchangehouse.service.impl;

import com.belatrix.exchangehouse.model.ExchangeCoins;
import com.belatrix.exchangehouse.repository.IExchangeCoinsRepository;
import com.belatrix.exchangehouse.service.IExchangeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ExchangeServiceImpl implements IExchangeService {

    private IExchangeCoinsRepository exchangeCoinsRepository;

    @Autowired
    public ExchangeServiceImpl(IExchangeCoinsRepository exchangeCoinsRepository) {
        this.exchangeCoinsRepository = exchangeCoinsRepository;
    }

    @Override
    public Optional<ExchangeCoins> getExchange(Integer idExchange) {
        Optional<ExchangeCoins> result = exchangeCoinsRepository.findById(idExchange);
        return result;
    }

    @Override
    public Optional<List<ExchangeCoins>> getCoinsToExchangeByBase(Integer baseId) {
        List<ExchangeCoins> exchangeCoins = exchangeCoinsRepository.getAllByBase(baseId);
        if (exchangeCoins == null)
            return Optional.empty();
        return Optional.of(exchangeCoins);
    }
}
