package com.belatrix.exchangehouse.service.impl;

import com.belatrix.exchangehouse.model.Transaction;
import com.belatrix.exchangehouse.repository.ITransactionsRepository;
import com.belatrix.exchangehouse.service.ITransactionService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class TransactionServiceImpl implements ITransactionService {

    private ITransactionsRepository transactionsRepository;

    @Autowired
    public TransactionServiceImpl(ITransactionsRepository transactionsRepository) {
        this.transactionsRepository = transactionsRepository;
    }

    @Async
    @Override
    public void saveTransaction(Transaction transaction) {
        Transaction transactionSaved = transactionsRepository.save(transaction);
        if(transaction != null)
            log.error("Couldn't save transaction");
    }

    @Override
    public List<Transaction> getAllTransactions() {
        return transactionsRepository.findAll();
    }
}
