package com.belatrix.exchangehouse.service;

import com.belatrix.exchangehouse.model.Transaction;

import java.util.List;

public interface ITransactionService {

    void saveTransaction(Transaction transactions);

    List<Transaction> getAllTransactions();
}
